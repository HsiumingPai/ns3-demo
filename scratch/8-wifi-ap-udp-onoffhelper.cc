#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/mobility-module.h"
#include "ns3/wifi-80211p-helper.h"
#include "ns3/network-module.h"
#include "ns3/wifi-module.h"
#include "ns3/wave-mac-helper.h"
#include "ns3/internet-module.h"
#include "ns3/applications-module.h"

#include <iostream>
using namespace std;
using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("5-rand-disc-position-demo");

void deployTopology (NodeContainer& nodes)
{	
	
	
	
	
  	MobilityHelper mob;
  	//////////////////////////////////////////////////////////  	
  	//// This is another nodes position allocation method
  	//// I guess X and Y are the center, and Rho is the radius
  	//////////////////////////////////////////////////////////
	mob.SetPositionAllocator ("ns3::RandomDiscPositionAllocator",
                                 "X", StringValue ("100.0"),
                                 "Y", StringValue ("100.0"),
                                 "Rho", StringValue ("ns3::UniformRandomVariable[Min=0|Max=30]"));
	
  	mob.SetMobilityModel ("ns3::RandomWalk2dMobilityModel",	
				"Mode", StringValue ("Time"),
				"Time", StringValue("100s"),
				"Speed", StringValue ("ns3::UniformRandomVariable[Min=1|Max=10]"),
				"Bounds", StringValue ("-500|500|-500|500"),
				"Direction", StringValue ("ns3::UniformRandomVariable[Min=0|Max=7]"));
	
	mob.Install (nodes);
}

NetDeviceContainer setupWifi (NodeContainer& wifi_nodes)
{
	//WI-Fi Helper
	
	WifiHelper wifi;
  	wifi.SetStandard (WIFI_PHY_STANDARD_80211a);
  	wifi.SetRemoteStationManager ("ns3::ArfWifiManager");

  	//PHY
  	YansWifiChannelHelper wifiChannel = YansWifiChannelHelper::Default ();	
	Ptr<YansWifiChannel> channel = wifiChannel.Create ();
	YansWifiPhyHelper phy =  YansWifiPhyHelper::Default ();
	phy.SetChannel (channel);
	
	double m_txp = 20;	
	phy.Set ("TxPowerStart",DoubleValue (m_txp));
  	phy.Set ("TxPowerEnd", DoubleValue (m_txp));

  	WifiMacHelper mac;
	mac.SetType ("ns3::StaWifiMac",
			"Ssid", SsidValue (Ssid ("wifi0")),
			"ActiveProbing", BooleanValue (true));
	
	NodeContainer sta_nodes;
	uint32_t last_node_index = wifi_nodes.GetN ()-1;
	for (uint32_t i = 0; i < last_node_index; ++i)
	{
		sta_nodes.Add (wifi_nodes.Get (i));
	}
	NetDeviceContainer nic_container = wifi.Install (phy, mac, sta_nodes);

	NodeContainer ap_nodes;	
	ap_nodes.Add (wifi_nodes.Get (last_node_index));
	mac.SetType ("ns3::ApWifiMac",
			"Ssid", SsidValue (Ssid ("wifi0")));
	
	nic_container.Add (wifi.Install (phy, mac, ap_nodes));

    return nic_container;
}

Ipv4InterfaceContainer setupIP (NodeContainer& nodes, NetDeviceContainer& net_container)
{
	InternetStackHelper	inet;
	inet.Install (nodes);
	Ipv4AddressHelper ipv4;
	ipv4.SetBase ("192.168.1.0",  "255.255.255.0");
	return ipv4.Assign (net_container);
}

void setupApplication (NodeContainer nodes, Ipv4InterfaceContainer ipv4_container)
{
	//SENDER
	NS_LOG_UNCOND ("start ");
	OnOffHelper onoff1 ("ns3::UdpSocketFactory",Address ());
  	onoff1.SetAttribute ("OnTime", StringValue ("ns3::ConstantRandomVariable[Constant=1.0]"));
  	onoff1.SetAttribute ("OffTime", StringValue ("ns3::ConstantRandomVariable[Constant=0.0]"));
  	uint16_t port = 80;
  	AddressValue remoteAddress (InetSocketAddress ("192.168.1.2", port));
	onoff1.SetAttribute ("Remote", remoteAddress);
	
	ApplicationContainer temp_app = onoff1.Install (nodes.Get (0));
	temp_app.Start (Seconds (1.0));
	temp_app.Stop (Seconds (30.0));

	
  	Address remote_addr (InetSocketAddress (Ipv4Address::GetAny (), port));
	PacketSinkHelper pkt_sinker ("ns3::UdpSocketFactory", remote_addr);	
	temp_app = pkt_sinker.Install (nodes.Get (1));
	temp_app.Start (Seconds (0.0));
	temp_app.Stop (Seconds (30.0));
}
int main(int argc, char* argv[])
{
	CommandLine cmd;
	cmd.Parse (argc, argv);
	NodeContainer nodes;
	int nodes_num = 3;
	nodes.Create (nodes_num);
	deployTopology (nodes);
	NetDeviceContainer nic_container = setupWifi (nodes);
	Ipv4InterfaceContainer ipv4_container = setupIP (nodes, nic_container);
	setupApplication (nodes, ipv4_container);
	Simulator::Stop (Seconds (100.0));
	Simulator::Run ();
  	Simulator::Destroy ();
	return 0;
}

