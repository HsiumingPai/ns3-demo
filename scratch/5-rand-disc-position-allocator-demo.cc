#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/mobility-module.h"

#include <iostream>
using namespace std;
using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("5-rand-disc-position-demo");

void deployTopology ()
{	
	NodeContainer nodes;
	nodes.Create (30);
	
	
  	MobilityHelper mob;
  	//////////////////////////////////////////////////////////  	
  	//// This is another nodes position allocation method
  	//// I guess X and Y are the center, and Rho is the radius
  	//////////////////////////////////////////////////////////
	mob.SetPositionAllocator ("ns3::RandomDiscPositionAllocator",
                                 "X", StringValue ("100.0"),
                                 "Y", StringValue ("100.0"),
                                 "Rho", StringValue ("ns3::UniformRandomVariable[Min=0|Max=30]"));
	
  	mob.SetMobilityModel ("ns3::RandomWalk2dMobilityModel",	
				"Mode", StringValue ("Time"),
				"Time", StringValue("100s"),
				"Speed", StringValue ("ns3::UniformRandomVariable[Min=10|Max=90]"),
				"Bounds", StringValue ("-500|500|-500|500"),
				"Direction", StringValue ("ns3::UniformRandomVariable[Min=0|Max=7]"));
	
	mob.Install (nodes);
}

int main(int argc, char* argv[])
{
	CommandLine cmd;
	cmd.Parse (argc, argv);

	deployTopology ();

	Simulator::Stop (Seconds (100.0));
	Simulator::Run ();
  	Simulator::Destroy ();
	return 0;
}

