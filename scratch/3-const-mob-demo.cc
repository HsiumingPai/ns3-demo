#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/mobility-module.h"

#include <iostream>
using namespace std;
using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("3-cont-mob-demo");

void deployTopology ()
{	
	NodeContainer nodes;
	nodes.Create (30);
	
	////////////////////////////////////////////////
	//// This is one of the way to allocate the 
	//// position of "a lot of nodes"
	//// 2 part, 1st is the allocation of 
	//// initialzation position of nodes, 2nd is 
	//// make it move (constant means stay fixed point)
	////
	//// MobilityHelper is not necessary for the node with position
	//// but very convenient to arrange a gross of nodes!
	//// If the node is assigned the position, it will show red circle
	//// in the pyviz, if not it will show gray circle
	//// position is not necessary for some network devices
	//////////////////////////////////////
	Ptr<ListPositionAllocator> nodes_position = CreateObject<ListPositionAllocator> ();	
	for (double x = 0; x < (nodes.GetN ())/2.0; ++x)
	{
		
		nodes_position->Add (Vector (x * 5.5, 0.0, 0.0));		
	}
	for (double y = 0; y < (nodes.GetN ())/2.0 + 1; ++y)
	{
		
		nodes_position->Add (Vector (0.0, y * 6.5, 0.0));		
	}

  	MobilityHelper mob;
  	mob.SetPositionAllocator (nodes_position);
	mob.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
	mob.Install (nodes);
}

int main(int argc, char* argv[])
{
	CommandLine cmd;
	cmd.Parse (argc, argv);

	deployTopology ();

	Simulator::Stop (Seconds (100.0));
	Simulator::Run ();
  	Simulator::Destroy ();
	return 0;
}

