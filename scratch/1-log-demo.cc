#include "ns3/core-module.h"
#include <iostream>
using namespace std;
using namespace ns3;
/* for showing the effect of the log macro */

// if you want to use the different level of log , you need 
// to give it a log component define name
// like this file, I define it as "log-demo"
NS_LOG_COMPONENT_DEFINE ("1-log-demo");

int main(int argc, char* argv[])
{
	// CommnadLine.parse is necessary if you want to use the
	// pyviz to debug (the GUI debugger)
	CommandLine cmd;
	cmd.Parse (argc, argv);
	
	// UNCOND show the log in any condition
	NS_LOG_UNCOND ("NS_LOG_UNCOND works!");

	//	This is a dynamic way to make the specific log component enabled.
	LogComponentEnable("log-demo", LOG_LEVEL_INFO);
	
	///////////////////////////////////////////////////
	///////////////////////////////////////////////////
	//// The following log is hierachical,
	//// e.g., (under shell)
	//// $export NS_LOG=log-demo=level_info
	//// you will see the ERROR, WARN, DEBUG but no FUNCTION AND LOGIC
	///////////////////////////////////////////////////
	///////////////////////////////////////////////////
    NS_LOG_ERROR ("NS_LOG_ERROR works!");
    NS_LOG_WARN ("NS_LOG_WARN works!");
    NS_LOG_DEBUG ("NS_LOG_DEBUG works!");
    NS_LOG_INFO ("NS_LOG_INFO works!");
    NS_LOG_FUNCTION ("NS_LOG_FUNCTION works!");
    NS_LOG_LOGIC ("NS_LOG_LOGIC works!");

    // just the normal cout, show msg anyway.
    cout << "cout works!" << endl;
	return 0;
}