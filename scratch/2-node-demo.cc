#include "ns3/core-module.h"
#include "ns3/network-module.h"

#include <iostream>
using namespace std;
using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("2-node-demo");

void deployTopology ()
{	
	NodeContainer nodes;
	nodes.Create (30);
}

int main(int argc, char* argv[])
{
	// CommnadLine.parse is necessary if you want to use the
	// pyviz to debug (the GUI debugger)
	CommandLine cmd;
	cmd.Parse (argc, argv);


	// All the ns3 script will execute between
	// cmd.Parse to Simulator::Stop
	// The main script is inside deployTopology subroutine
	deployTopology ();

	Simulator::Stop (Seconds (44.0));
	Simulator::Run ();
  	Simulator::Destroy ();
	return 0;
}

