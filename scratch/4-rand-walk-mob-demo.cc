#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/mobility-module.h"

#include <iostream>
using namespace std;
using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("4-rand-walk-mob-demo");

void deployTopology ()
{	
	NodeContainer nodes;
	nodes.Create (30);
	
	Ptr<ListPositionAllocator> nodes_position = CreateObject<ListPositionAllocator> ();	
	
	for (double y = 0; y < (nodes.GetN ())/1.0; ++y)
	{
		
		nodes_position->Add (Vector (0.0, y * 3.0, 0.0));		
	}

  	MobilityHelper mob;
  	mob.SetPositionAllocator (nodes_position);
  	//////////////////////////////////////////////////////////
  	//// This is a another mobility helper
  	//// You can make all of the node move at a random variable speed.  
  	//// I'm not sure the meaning of the parameter but 
  	//// I just can guess the speed is a random variable (here is constant)
  	//// the Bound is the node is limited in a range, 
  	//// parameters are (x min, x max, y min y max)
  	//// Direction is also the random variable, the unit is 
  	//// radian but not degree (e.g, constant zero means go along 
	//// the x-axis direction
	//////////////////////////////////////////////////////////
	//// If you would like to make it walk in a variety of speed or direction 
	//// maybe try "ns3::UniformRandomVariable[Min=0.0|Max=300.0]" as the 
	//// parameter
  	//////////////////////////////////////////////////////////
  	//////////////////////////////////////////////////////////
  	mob.SetMobilityModel ("ns3::RandomWalk2dMobilityModel",	
				"Mode", StringValue ("Time"),
				"Time", StringValue("10s"),
				"Speed", StringValue ("ns3::ConstantRandomVariable[Constant=54.0]"),
				"Bounds", StringValue ("-100|100|-100|100"),
				"Direction", StringValue ("ns3::ConstantRandomVariable[Constant=0.0]"));
	
	mob.Install (nodes);
}

int main(int argc, char* argv[])
{
	CommandLine cmd;
	cmd.Parse (argc, argv);

	deployTopology ();

	Simulator::Stop (Seconds (100.0));
	Simulator::Run ();
  	Simulator::Destroy ();
	return 0;
}

